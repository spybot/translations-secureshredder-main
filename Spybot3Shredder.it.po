msgid ""
msgstr ""
"PO-Revision-Date: 2024-02-26 08:18+0000\n"
"Last-Translator: DeepL <noreply-mt-deepl@weblate.org>\n"
"Language-Team: Italian <http://translations.spybot.de/projects/"
"spybot-secure-shredder/spybot-secure-shredder/it/>\n"
"Language: it\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=n != 1;\n"
"X-Generator: Weblate 5.3\n"

#: shredder.scheduledtask.rsscheduledtaskdisplayname
msgid "Start Shredder"
msgstr "Avviare il trituratore"

#: shredder.scheduledtask.rsscheduledtaskinfodescription
msgid "This task will start the Shredder to allow you cleaning up."
msgstr "Questa attività avvierà il Tritacarne per consentire la pulizia."

#: shredder.ui.frame.main.rssecureshredderactionadd
msgid "Add file(s)..."
msgstr "Aggiungere file..."

#: shredder.ui.frame.main.rssecureshredderactionshowinstalledapps
msgctxt "shredder.ui.frame.main.rssecureshredderactionshowinstalledapps"
msgid "Installed Apps"
msgstr "Applicazioni installate"

#: shredder.ui.frame.main.rssecureshredderactiontemplates
msgid "Add more..."
msgstr "Aggiungi altro..."

#: shredder.ui.frame.main.rssecureshredderdetails
msgid "Details"
msgstr "Dettagli"

#: shredder.ui.frame.main.rssecureshredderframename
msgid "Spybot Secure Shredder"
msgstr "Spybot Secure Shredder"

#: shredder.ui.frame.main.rssecureshreddershreddedfilescountthis
msgid "Number of files shredded in this session"
msgstr "Numero di file distrutti in questa sessione"

#: shredder.ui.frame.main.rssecureshreddershreddedfilescounttotal
msgid "Total count of shredded files"
msgstr "Numero totale di file distrutti"

#: shredder.ui.frame.main.rssecureshredderunabletoaddfile
#, object-pascal-format
msgid "Unable to add file \"%s\" to the file scanner."
msgstr "Impossibile aggiungere il file \"%s\" alla scansione dei file."

#: shredder.ui.frame.main.rssecureshredderwheelfiles
msgid "Shredder"
msgstr "Trituratore"

#: shredder.ui.frame.main.rssecureshredderwheelsuggestion
msgid "Drop file (or click)"
msgstr "Rilasciare il file (o fare clic)"

#: shredder.ui.frame.results.rsshredderresultscaption
msgid "Secure File Shredder Results"
msgstr "Risultati della distruzione sicura dei file"

#: shredder.ui.frame.results.rsshredderresultscategoryfailure
msgid "Shredding failed"
msgstr "Triturazione fallita"

#: shredder.ui.frame.results.rsshredderresultscategoryqueue
msgid "Queued for shredding"
msgstr "In coda per la triturazione"

#: shredder.ui.frame.results.rsshredderresultscategorysuccess
msgid "Successfully shredded"
msgstr "Triturato con successo"

#: shredder.ui.frame.results.rsshredderresultsfilename
msgid "Filename"
msgstr "Nome del file"

#: shredder.ui.frame.results.rsshredderresultsfiles
msgid "Files"
msgstr "File"

#: shredder.ui.frame.results.rsshredderresultsheadertext
msgid "files processed"
msgstr "file elaborati"

#: shredder.ui.frame.results.rsshredderresultsstatus
msgid "Status"
msgstr "Stato"

#: shredder.ui.frame.results.rsshredderresultsstatusfailed
msgid "failed"
msgstr "fallito"

#: shredder.ui.frame.results.rsshredderresultsstatusqueued
msgid "queued"
msgstr "in coda"

#: shredder.ui.frame.results.rsshredderresultsstatusshredded
msgid "shredded"
msgstr "triturati"

#: shredder.ui.frame.templates.files.rsframeshreddertemplatefilescaption
msgid "Spybot Secure Shredder - Template Files"
msgstr "Spybot Secure Shredder - File modello"

#: shredder.ui.frame.templates.files.rsframeshreddertemplatefilestableheader
#, object-pascal-format
msgid "files found in %0:s"
msgstr "file che si trovano in %0:s"

#: shredder.ui.frame.templates.files.rsframeshreddertemplatefilestableheadersimple
msgid "files found"
msgstr "file trovati"

#: shredder.ui.frame.templates.rsactionapply
msgid "Apply"
msgstr "Applica"

#: shredder.ui.frame.templates.rsactioncancel
msgid "Cancel"
msgstr "Annullamento"

#: shredder.ui.frame.templates.rscategorybrowserfiles
msgid "Browser Files"
msgstr "File del browser"

#: shredder.ui.frame.templates.rscategorysystemfiles
msgid "System Files"
msgstr "File di sistema"

#: shredder.ui.frame.templates.rscategoryuserfiles
msgid "User Files"
msgstr "File utente"

#: shredder.ui.frame.templates.rselevationrequired
msgid "click to continue as admin"
msgstr "fare clic per continuare come amministratore"

#: shredder.ui.frame.templates.rsshreddertemplatesavailable
msgid "templates available"
msgstr "modelli disponibili"

#: shredder.ui.frame.templates.rsshreddertemplatesframename
msgid "Spybot Secure Shredder - Templates"
msgstr "Spybot Secure Shredder - Modelli"

#: shredder.ui.frame.templates.rsshreddertemplatesoff
msgid "Ignore"
msgstr "Ignorare"

#: shredder.ui.frame.templates.rsshreddertemplateson
msgid "Shred"
msgstr "Triturare"

#: shredder.ui.settings.rsidentitymonitorsettingsscheduledtaskcaption
msgid "Re-test for breaches at each Logon (via Scheduled Task)"
msgstr ""
"Eseguire nuovamente il test delle violazioni ad ogni accesso (tramite "
"un'attività pianificata)."

#: shredder.ui.settings.rsidentitymonitorsettingsupgradecheck
msgid "Check for updates on program start"
msgstr "Verifica degli aggiornamenti... all'avvio del programma."

#: sysinfo.ui.frame.installedapps.rsframenameinstalledapps
msgctxt "sysinfo.ui.frame.installedapps.rsframenameinstalledapps"
msgid "Installed Apps"
msgstr "Applicazioni installate"

#: tformspybot3shredder.caption
msgid "Spybot Shredder"
msgstr "Spybot Shredder"
